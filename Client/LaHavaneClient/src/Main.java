
 
import java.io.IOException;
import java.net.URL;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
 
public class Main extends Application {
    public static void main(String[] args) {
        launch(args);
    }
    
    @Override
    public void start(Stage primaryStage) {
    	try {
    	      // Localisation du fichier FXML.
    	      final URL url = getClass().getResource("FXML/MainFrame.fxml");
    	      // Cr�ation du loader.
    	      final FXMLLoader fxmlLoader = new FXMLLoader(url);
    	      // Chargement du FXML.
    	      final AnchorPane root = (AnchorPane) fxmlLoader.load();
    	      // Cr�ation de la sc�ne.
    	      final Scene scene = new Scene(root, 300, 250);
    	      primaryStage.setScene(scene);
    	    } catch (IOException ex) {
    	      System.err.println("Erreur au chargement: " + ex);
    	    }
    	    primaryStage.setTitle("Test FXML");
    	    primaryStage.show();
    }
}