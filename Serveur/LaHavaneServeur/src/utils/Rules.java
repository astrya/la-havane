package utils;

public class Rules {
	
	private Rules() {}
	
	private static Rules rules = null;
	
	private int nbVictoryPointToReach;	
	private int nbRowRevealedBuildings;
	private int nbRevealedBuildingsByRow;
	private int nbCoinAtBoardAtStart;
	
	public static Rules getRules()
    {           
        if (rules == null)
        {   rules = new Rules(); 
        }
        return rules;
    }
	
	public Rules setRules(int nbVictoryPoint, int nbRowRevealedBuildings, int nbRevealedBuildingsByRow, int nbCoinAtBoardAtStart)
    {           
        if (rules == null)
        {   
        	this.nbVictoryPointToReach = nbVictoryPoint;
        	this.nbRowRevealedBuildings = nbRowRevealedBuildings;
        	this.nbRevealedBuildingsByRow = nbRevealedBuildingsByRow;
        	this.nbCoinAtBoardAtStart = nbCoinAtBoardAtStart;
        	
        	rules = new Rules();
        }
        return rules;
    }
}