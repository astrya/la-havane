package action;

public class Action {
	private String name;
	private String description;
	private String effet;
	
	public Action(String name, String description, String effet) {
		this.name = name;
		this.description = description;
		this.effet = effet;
	}
	
	public void playAction() {}
	
	
}
