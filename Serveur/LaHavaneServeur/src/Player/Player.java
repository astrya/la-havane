package Player;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.EnumMap;

import action.Action;
import utils.EnumResources;

public class Player {
	private String colors;
	EnumMap<EnumResources, Integer> inventory = new EnumMap<>(EnumResources.class);
	ArrayList<Action> actionCard;
	
	public Player(String colors, EnumMap<EnumResources, Integer> inventory, ArrayList<Action> actionCard) {
		this.colors = colors;
		System.out.println("CREATION DU JOUEUR : ");
		System.out.println(this.colors);
		this.actionCard = new ArrayList<Action>(actionCard);
		this.inventory = new EnumMap<EnumResources, Integer>(inventory);
	}

	public EnumMap<EnumResources, Integer> getInventory() {
		return inventory;
	}

	public void setInventory(EnumMap<EnumResources, Integer> inventory) {
		this.inventory = inventory;
	}
	
	
	
}
