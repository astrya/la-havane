package gameMaster;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import building.Building;
import utils.EnumResources;

public class Board {
	
	// Reserve du jeu pour les ressources
	private EnumMap<EnumResources, Integer> reserveResources;
	// Ressources au centre du plateau
	private EnumMap<EnumResources, Integer> boardResources;
	
	// Reserve du jeu pour les batiments
	private ArrayList<Building> reserveBuildings;
	// Premiere rangee de 6 batiments max
	private ArrayList<Building> firstRow;
	// Seconde rangee de 6 batiments max
	private ArrayList<Building> secondRow;
	
	public Board() {
		reserveResources = new EnumMap<EnumResources, Integer>(EnumResources.class);
		boardResources = new EnumMap<EnumResources, Integer>(EnumResources.class);
		
		reserveBuildings = new ArrayList<>();
		firstRow = new ArrayList<>();
		secondRow = new ArrayList<>();
		
		initBoard();
	}
	
	public Board(EnumMap<EnumResources, Integer> reserveResources, List<Building> reserveBuildings) {
		this.reserveResources = new EnumMap<EnumResources, Integer>(reserveResources);
		boardResources = new EnumMap<EnumResources, Integer>(EnumResources.class);
		
		this.reserveBuildings = new ArrayList<>(reserveBuildings);
		firstRow = new ArrayList<>();
		secondRow = new ArrayList<>();
	}
	
	public void initBoard() {
		reserveResources.replace(EnumResources.Pesos, 60);
		reserveResources.replace(EnumResources.Worker, 15);
		
		reserveResources.replace(EnumResources.Debris, 40);
		reserveResources.replace(EnumResources.Brick, 10);
		reserveResources.replace(EnumResources.Sandstone, 10);
		reserveResources.replace(EnumResources.Loam, 10);
		reserveResources.replace(EnumResources.Glass, 10);
	}
	
	public void initPutInRows() {
		int maxRow = 6;
		int i = 0;
		while(!reserveBuildings.isEmpty() && i < maxRow) {
			Building b1 = reserveBuildings.get(0);
			firstRow.add(b1);
			reserveBuildings.remove(b1);
			
			Building b2 = reserveBuildings.get(0);
			firstRow.add(b2);
			reserveBuildings.remove(b2);
			
			i++;
		}
	}
	
	
	
}