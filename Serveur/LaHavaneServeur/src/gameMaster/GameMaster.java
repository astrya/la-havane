package gameMaster;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import building.Building;
import utils.EnumResources;

public class GameMaster {
	
	private Board board;
	
	public GameMaster() {
		board = new Board();
		initGame();
	}
	
	public GameMaster(EnumMap<EnumResources, Integer> reserveResources, List<Building> reserveBuildings) {
		board = new Board(reserveResources,reserveBuildings);
		initGame();
	}
	
	private void initGame() {
		board.initPutInRows();
	}
	
}
