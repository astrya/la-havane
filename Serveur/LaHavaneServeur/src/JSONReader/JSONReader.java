package JSONReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import utils.EnumResources;
import action.*;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import Player.Player;
import building.Building;


public class JSONReader {
	
	private List<Building> buildingList;
	private EnumMap<EnumResources, Integer> resourcesMap;
	private ArrayList<Action> actionsList;
	
	public JSONReader() {
		System.out.println("Lancement lecture du fichier json");
		startReadJSON(2);
	}
	
	private void startReadJSON(int numberPlayer) {
		
		JSONParser parser = new JSONParser();
		try {     
            Object obj = parser.parse(new FileReader("../../resources/Resources_game_elements-Havana.json"));

            JSONObject jsonObject =  (JSONObject) obj;

            String game_name = (String) jsonObject.get("game_name");
            System.out.println(game_name);

            String n_min_players = (String) jsonObject.get("n_min_players");
            System.out.println(n_min_players);

            String n_max_players = (String) jsonObject.get("n_max_players");
            System.out.println(n_max_players);
            
            // ---------------Resources---------------
            JSONObject resources = (JSONObject) jsonObject.get("resources");
            
            resourcesMap = new EnumMap<EnumResources, Integer>(EnumResources.class);
            
            // ---------------Worker---------------
            JSONObject worker = (JSONObject) resources.get("worker");
            System.out.println(worker.get("name"));
            System.out.println(worker.get("number"));
            int workerNumber = Integer.parseInt(worker.get("number").toString());
            resourcesMap.replace(EnumResources.Worker, workerNumber);
            
            // ---------------Coin---------------
            JSONObject coin = (JSONObject) resources.get("coin");
            System.out.println(coin.get("name"));
            System.out.println(coin.get("number"));
            int coinNumber = Integer.parseInt(coin.get("number").toString());
            resourcesMap.replace(EnumResources.Worker, coinNumber);
            
            // ---------------Material---------------
            JSONArray materials = (JSONArray) resources.get("materials");
            for(Object material : materials) {
                JSONObject mat = (JSONObject) material;
                System.out.println("/MATERIAL\\");
            	System.out.println(mat.get("is_colored"));
            	System.out.println(mat.get("name"));
            	System.out.println(mat.get("color"));
            	System.out.println(mat.get("number"));
            	String matString = mat.get("name").toString();
            	matString = matString.substring(0, 1).toUpperCase() + matString.substring(1);
            	int matNumber = Integer.parseInt(mat.get("number").toString());
            	resourcesMap.replace(EnumResources.valueOf(matString), matNumber);
            }
            
            // ---------------Building---------------
            buildingList = new ArrayList<Building>();
            
            JSONArray buildings = (JSONArray) jsonObject.get("buildings");
            for(Object building : buildings) {
                JSONObject build = (JSONObject) building;
                System.out.println("/BUILDING\\");
                String is_architect_required = (String) build.get("is_architect_required");
                
                
            	System.out.println(is_architect_required);
            	System.out.println(build.get("n_victory_pts"));
                JSONObject cost = (JSONObject) build.get("cost");
                System.out.println(cost.get("n_coins"));
                System.out.println(cost.get("n_material_brick"));
                System.out.println(cost.get("n_material_debris"));
                System.out.println(cost.get("n_material_glass"));
                System.out.println(cost.get("n_material_loam"));
                System.out.println(cost.get("n_material_sandstone"));
                System.out.println(cost.get("n_workers"));
                
                EnumMap<EnumResources, Integer> costMap = new EnumMap<>(EnumResources.class);
                costMap.put(EnumResources.Pesos, Integer.parseInt((String) cost.get("n_coins")));
                costMap.put(EnumResources.Worker, Integer.parseInt((String) cost.get("n_workers")));
                costMap.put(EnumResources.Brick, Integer.parseInt((String) cost.get("n_material_brick")));
                costMap.put(EnumResources.Glass, Integer.parseInt((String) cost.get("n_material_glass")));
                costMap.put(EnumResources.Loam, Integer.parseInt((String) cost.get("n_material_loam")));
                costMap.put(EnumResources.Sandstone, Integer.parseInt((String) cost.get("n_material_sandstone")));
                costMap.put(EnumResources.Debris, Integer.parseInt((String) cost.get("n_material_debris")));
                
                buildingList.add(new Building(Integer.parseInt((String) build.get("n_victory_pts")),
                		Boolean.parseBoolean((String) build.get("is_architect_required")),
                		costMap));
            }
            
            // ---------------Actions---------------
            actionsList = new ArrayList<Action>(); 
            JSONArray actions = (JSONArray) jsonObject.get("actions");
            for(Object action : actions) {
                JSONObject act = (JSONObject) action;
                System.out.println("/ACTION\\");
            	System.out.println(act.get("can_be_skipped"));
            	System.out.println(act.get("name"));
            	System.out.println(act.get("text"));
            	System.out.println(act.get("value"));
            	switch((String) act.get("name")){
            		case "Siesta":
                    	Action siesta = new Siesta((String) act.get("name"), (String) act.get("text"), "");
                    	actionsList.add(siesta);
            		    break;
            		case "Refreshment":
                    	Action refreshment = new Refreshment((String) act.get("name"), (String) act.get("text"), "");
                    	actionsList.add(refreshment);
            		    break;
            		case "Protection":
                    	Action protection = new Protection((String) act.get("name"), (String) act.get("text"), "");
                    	actionsList.add(protection);
            		    break;
            		case "Debris":
                    	Action debris = new Debris((String) act.get("name"), (String) act.get("text"), "");
                    	actionsList.add(debris);
            		    break;
            		case "Conservation":
            			Action conservation = new Conservation((String) act.get("name"), (String) act.get("text"), "");
            			actionsList.add(conservation);
            		    break;
            		case "Tax Collector":
            			Action taxCollector = new TaxCollector((String) act.get("name"), (String) act.get("text"), "");
            			actionsList.add(taxCollector);
            		    break;
            		case "Worker":
            			Action workerAction = new Worker((String) act.get("name"), (String) act.get("text"), "");
            			actionsList.add(workerAction);
            		    break;
            		case "Architect":
            			Action architect = new Architect((String) act.get("name"), (String) act.get("text"), "");
            			actionsList.add(architect);
            		    break;
            		case "Pesos Thief":
            			Action pesosThief = new PesosThief((String) act.get("name"), (String) act.get("text"), "");
            			actionsList.add(pesosThief);
            		    break;
            		case "Materials Thief":
            			Action materialsThief = new MaterialsThief((String) act.get("name"), (String) act.get("text"), "");
            			actionsList.add(materialsThief);
            		    break;
            		case "Black Market":
            			Action blackMarket = new BlackMarket((String) act.get("name"), (String) act.get("text"), "");
            			actionsList.add(blackMarket);
            		    break;
            		case "Pesos":
            			Action pesosAction = new Pesos((String) act.get("name"), (String) act.get("text"), "");
            			actionsList.add(pesosAction);
            		    break;
            		case "Mama":
            			Action mama = new Mama((String) act.get("name"), (String) act.get("text"), "");
            			actionsList.add(mama);
            		    break;
            		default:
            		    // code block
            		}       	
            }
            
            // ---------------Victory point to win---------------
            JSONArray n_victory_pts_to_reach_for_players = (JSONArray) jsonObject.get("n_victory_pts_to_reach_for_players");
            for(Object n_victory_pts_to_reach_for_player : n_victory_pts_to_reach_for_players) {
                JSONObject pts_to_reach = (JSONObject) n_victory_pts_to_reach_for_player;
                System.out.println("/n_victory_pts_to_reach_for_players\\");
            	System.out.println(pts_to_reach.get("n_players"));
            	System.out.println(pts_to_reach.get("n_victory_pts_to_reach"));
            }
            
            // ---------------Preparation---------------
            JSONObject preparation = (JSONObject) jsonObject.get("preparation");
            System.out.println("/PREPARATION\\");
            System.out.println(preparation.get("n_actions"));
            JSONObject centerTable = (JSONObject) preparation.get("center_table");
            System.out.println("Pour la table : ");
            System.out.println(centerTable.get("n_coins"));
            System.out.println(centerTable.get("n_draw_materials"));
            JSONObject revealed_buildings = (JSONObject) centerTable.get("revealed_buildings");
            System.out.println(revealed_buildings.get("n_revealed_buildings_by_row"));
            System.out.println(revealed_buildings.get("n_rows_revealed_buildings"));
            System.out.println("Pour le joueur : ");
            JSONObject player = (JSONObject) preparation.get("player");
            System.out.println(player.get("n_coins"));           
            System.out.println(player.get("n_material_brick"));
            System.out.println(player.get("n_material_debris"));
            System.out.println(player.get("n_material_glass"));
            System.out.println(player.get("n_material_loam"));
            System.out.println(player.get("n_material_sandstone"));
            System.out.println(player.get("n_workers"));
            
            //Nb de material a piocher premier tour
            System.out.println(player.get("n_draw_materials"));
            
            EnumMap<EnumResources, Integer> playerStartInventory = new EnumMap<>(EnumResources.class);
            playerStartInventory.put(EnumResources.Pesos, Integer.parseInt((String) player.get("n_coins")));
            playerStartInventory.put(EnumResources.Worker, Integer.parseInt((String) player.get("n_workers")));
            playerStartInventory.put(EnumResources.Brick, Integer.parseInt((String) player.get("n_material_brick")));
            playerStartInventory.put(EnumResources.Glass, Integer.parseInt((String) player.get("n_material_glass")));
            playerStartInventory.put(EnumResources.Loam, Integer.parseInt((String) player.get("n_material_loam")));
            playerStartInventory.put(EnumResources.Sandstone, Integer.parseInt((String) player.get("n_material_sandstone")));
            playerStartInventory.put(EnumResources.Debris, Integer.parseInt((String) player.get("n_material_debris")));
            
            // ---------------course---------------
            JSONObject course = (JSONObject) jsonObject.get("course");
            System.out.println("/PHASE\\");
            JSONObject phase_actions_and_purchasing = (JSONObject) course.get("phase_actions_and_purchasing");
            System.out.println("/phase 1 : \\");
            JSONObject is_cost_after_purchasing_removed = (JSONObject) phase_actions_and_purchasing.get("is_cost_after_purchasing_removed");
            System.out.println(is_cost_after_purchasing_removed.get("is_coin_removed"));
            System.out.println(is_cost_after_purchasing_removed.get("is_material_removed"));
            System.out.println(is_cost_after_purchasing_removed.get("is_worker_removed"));
            JSONObject swap_during_purchasing = (JSONObject) phase_actions_and_purchasing.get("swap_during_purchasing");
            System.out.println(swap_during_purchasing.get("n_coins_for_1_worker"));
            System.out.println(swap_during_purchasing.get("n_material_debris_for_1_colored_material"));
            JSONObject phase_supplies = (JSONObject) course.get("phase_supplies");
            System.out.println("/phase 2 : \\");
            System.out.println(phase_supplies.get("n_coins"));
            System.out.println(phase_supplies.get("n_draw_materials"));
            JSONObject phase_new_action_card = (JSONObject) course.get("phase_new_action_card");
            System.out.println("/phase 3 : \\");
            System.out.println(phase_new_action_card.get("n_min_actions_in_hand"));
            System.out.println(phase_new_action_card.get("n_new_actions"));
            
            
            // ---------------COLOR---------------
            JSONArray playerColors = (JSONArray) jsonObject.get("player_colors");
            String[] colors = new String[4];
            int cpt = 0;
            for(Object color : playerColors) {
            	System.out.println(color);
            	colors[cpt] = (String) color;
            	cpt++;
            }
            
            ArrayList<Player> players = new ArrayList<Player>();            
            if(numberPlayer == 2) {
            	players.add(new Player(colors[0], playerStartInventory, actionsList));
            	players.add(new Player(colors[1], playerStartInventory, actionsList));
            }
            else if(numberPlayer == 3){
            	players.add(new Player(colors[0], playerStartInventory, actionsList));
            	players.add(new Player(colors[1], playerStartInventory, actionsList));
            	players.add(new Player(colors[2], playerStartInventory, actionsList));
            }
            else {
            	players.add(new Player(colors[0], playerStartInventory, actionsList));
            	players.add(new Player(colors[1], playerStartInventory, actionsList));
            	players.add(new Player(colors[2], playerStartInventory, actionsList));
            	players.add(new Player(colors[3], playerStartInventory, actionsList));
            }
            
            

        } catch (Exception e) {
            e.printStackTrace();
        }
	}

	public List<Building> getBuildingList() {
		return buildingList;
	}

	public EnumMap<EnumResources, Integer> getResourcesMap() {
		return resourcesMap;
	}

	public ArrayList<Action> getActionsList() {
		return actionsList;
	}
	
	
	
}
