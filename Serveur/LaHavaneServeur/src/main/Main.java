package main;
import JSONReader.*;
import gameMaster.GameMaster;

public class Main {

	public static void main(String[] args) {
		System.out.println("Lancement de l'application");
		JSONReader xmlReader = new JSONReader();
		
		GameMaster gameMaster = new GameMaster(xmlReader.getResourcesMap(), xmlReader.getBuildingList());
	}

}
